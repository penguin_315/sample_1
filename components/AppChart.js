Vue.component('penguin-list', {
    template: `
            <div class="pure-menu">
                <a class="pure-menu-heading" href="javascript:void(0);">ユニット</a>

                <ul class="pure-menu-list">
                    <li class="pure-menu-item" v-on:click="target='edocco'"><a href="javascript:void(0);" class="pure-menu-link">EDO-CCO<small>(すみだ水族館)</small></a></li>
                    <li class="pure-menu-item" v-on:click="target='miyacco'"><a href="javascript:void(0);" class="pure-menu-link">MIYA-CCO<small>(京都水族館)</small></a></li>
                </ul>
                <div v-if="target==='edocco'">
                    <a class="pure-menu-heading" href="javascript:void(0);">EDO-CCO(すみだ水族館)</a>
                    <ul class="pure-menu-list">
                        <li class="pure-menu-item" v-for="penguin in edocco_member" :key="penguin.name"　v-on:click="t_pen=penguin.name">
                            <a href="javascript:void(0);" class="pure-menu-link" v-cloak>  
                                {{penguin.name}}
                                &nbsp;<small>{{penguin.catch}}</small>
                            </a>
                        </li>
                    </ul>
                </div>
                <div v-if="target==='miyacco'">
                    <ul class="pure-menu-list">
                        <a class="pure-menu-heading" href="javascript:void(0);">MIYA-CCO(京都水族館)</a>
                        <li class="pure-menu-item" v-for="penguin in miyacco_member" :key="penguin.name"　v-on:click="t_pen=penguin.name">
                            <a href="javascript:void(0);" class="pure-menu-link" v-cloak>
                                {{penguin.name}}
                                &nbsp;<small>{{penguin.catch}}</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        `,
    data() {
        return{
        t_pen: '',
        target: 'miyacco',
        edocco_member: [
            {
                id: 'matsuri_oshipen',
                name: 'まつり',
                catch: '甘えんぼお嬢様'
            },
            {
                id: 'hanabi_oshipen',
                name: 'はなび',
                catch: '新人スタッフの登竜門'
            },
            {
                id: 'happi_oshipen',
                name: 'はっぴ',
                catch: 'お豆腐メンタル'
            },
            {
                id: 'taiko_oshipen',
                name: 'たいこ',
                catch: 'ダメンズ大好き'
            },
            {
                id: 'fuurin_oshipen',
                name: 'ふうりん',
                catch: 'イワシへのこだわり'
            },
            {
                id: 'wasshoi_oshipen',
                name: 'わっしょい',
                catch: '同級生の前では強気'
            },
            {
                id: 'anmitsu_oshipen',
                name: 'あんみつ',
                catch: 'ちびっこギャング'
            },
            {
                id: 'azuma_oshipen',
                name: 'あずま',
                catch: '守ってあげたい王子様'
            },
            {
                id: 'anko_oshipen',
                name: 'あんこ',
                catch: '1000年に1羽の美少女'
            },
            {
                id: 'chouchin_oshipen',
                name: 'ちょうちん',
                catch: '癒やしのぽっちゃりさん'
            },
            {
                id: 'kiriko_oshipen',
                name: 'きりこ',
                catch: '気品あふれる1歳児'
            }
        ],
        miyacco_member: [
            {
                id: 'shichijo_oshipen',
                name: 'しちじょう',
                catch: 'とどまる事なき好奇心'
            },
            {
                id: 'osshi_oshipen',
                name: 'おっしー',
                catch: '最速の逃げ足'
            },
            {
                id: 'take_oshipen',
                name: 'たけ',
                catch: 'ドのつくツンデレ'
            },
            {
                id: 'maru_oshipen',
                name: 'まる',
                catch: 'ときどき自宅警備員'
            },
            {
                id: 'ane_oshipen',
                name: 'あね',
                catch: '猛烈フードファイター'
            },
            {
                id: 'shijo_oshipen',
                name: 'しじょう',
                catch: 'ハンサムなイクメン'
            },
            {
                id: 'shinmachi_oshipen',
                name: 'しんまち',
                catch: 'ペンギンより人が好き'
            },
            {
                id: 'tera_oshipen',
                name: 'てら',
                catch: '魔性のクールビューティー'
            },
            {
                id: 'aya_oshipen',
                name: 'あや',
                catch: '父の一撃'
            },
            {
                id: 'hachi_oshipen',
                name: 'はち',
                catch: 'ザ肝っ玉母ちゃん'
            },
            {
                id: 'sen_oshipen',
                name: 'せん',
                catch: '鳥類だけど一匹狼'
            },
        ]}
    },
    watch: {
        t_pen: function (val) {
            DrawPenguinChart(val),
            toggleAll()
        }
    }


});

function DrawPenguinChart(t_pen) {
    setTimeout(function () {
        var penguin_url = 'http://localhost/get_penguin/get_penguin.php?t=' + t_pen;
        console.log(penguin_url);
        var chart = c3.generate({
            bindto: '#chart',
            data: {
                x: 'datetime',
                xFormat: '%Y/%m/%d/%H:%M',
                url: penguin_url,
                mimeType: 'json',
                type: 'bar',
                labels: true,
                colors: {
                    'iine': '#b8e6e6'
                    // '順位': '#708090' 
                }
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        culling: {
                            max: 10
                        },
                        format: '%m/%d',
                    }
                }
            },
        });
    }, 1000);
}

function toggleAll(){
    var ua = navigator.userAgent;
    if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
        var layout = document.getElementById('layout'),
            menu = document.getElementById('menu'),
            menuLink = document.getElementById('menuLink'),
            content = document.getElementById('main');

        var active = 'active';

        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);      
    }
}

function toggleClass(element, className) {
    var classes = element.className.split(/\s+/),
        length = classes.length,
        i = 0;

    for (; i < length; i++) {
        if (classes[i] === className) {
            classes.splice(i, 1);
            break;
        }
    }
    // The className is not found
    if (length === classes.length) {
        classes.push(className);
    }

    element.className = classes.join(' ');
}

// 要素にマウントする
//menuRoot.$mount('#PenguinList');
