Vue.component('penguin-list', {
    props: {
        title:{
            type: String
        },
        desc: {
            type: String
        },
        target: {
            type: String
        },
        edocco_member: {
            type: Object
        },
        miyacco_member: {
            type: Object
        }
    },
    template: `
                <div class="pure-menu">
                    <a class="pure-menu-heading home" href="javascript:void(0);" v-on:click="$emit('reset'),toggleAll()"><i class="fas fa-home fa-fw"></i>TOPへ</a>
                    <a class="pure-menu-heading" href="javascript:void(0);">ペンギンユニット</a>
                    <ul class="pure-menu-list">
                        <li class="pure-menu-item" v-on:click="target='edocco'"><a href="javascript:void(0);" class="pure-menu-link">EDO-CCO<small>(すみだ水族館)</small></a></li>
                        <li class="pure-menu-item" v-on:click="target='miyacco'"><a href="javascript:void(0);" class="pure-menu-link">MIYA-CCO<small>(京都水族館)</small></a></li>
                    </ul>
                    <div v-if="target==='edocco'">
                        <a class="pure-menu-heading edocco" href="javascript:void(0);">EDO-CCO(すみだ水族館)</a>
                        <ul class="pure-menu-list">
                            <li class="pure-menu-item" v-for="penguin in edocco_member" :key="penguin.name"　v-on:click="t_pen=penguin.name,t_desc=penguin.catch,$emit('chart', penguin.name)">
                                <a href="javascript:void(0);" class="pure-menu-link" v-cloak>  
                                    <i class="fas fa-caret-right fa-fw"></i>{{penguin.name}}
                                    &nbsp;<small>{{penguin.catch}}</small>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div v-if="target==='miyacco'">
                        <ul class="pure-menu-list">
                            <a class="pure-menu-heading miyacco" href="javascript:void(0);">MIYA-CCO(京都水族館)</a>
                            <li class="pure-menu-item" v-for="penguin in miyacco_member" :key="penguin.name"　v-on:click="t_pen=penguin.name,t_desc=penguin.catch,$emit('chart', penguin.name)">
                                <a href="javascript:void(0);" class="pure-menu-link" v-cloak>
                                    <i class="fas fa-caret-right fa-fw"></i>{{penguin.name}}
                                    &nbsp;<small>{{penguin.catch}}</small>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
        `,
    data() {
        return{
            t_pen: '',
            t_desc: '',
            target: ''
        }
    },
    watch: {
        t_pen: function (val) {
            //DrawPenguinChart(val),
            toggleAll(),
            this.$parent.penguin_name = val;
            this.$emit('myrank', val);
        },
        t_desc: function(val){
            this.$parent.penguin_catch = "~" + val + "~";
        },
        target: function(val){
            this.$parent.target = val;
            this.$parent.target_rank = '';
            this.$parent.target_insta_rank = '';
            this.$parent.penguin_name = '';
            this.$parent.penguin_catch = '';

            if (val === 'edocco'){
                this.$parent.title = "EDO-CCO(すみだ水族館)";
                this.$parent.desc = "すみだ水族館のマゼランペンギンによるアイドルユニット";
            } else if (val === 'miyacco'){
                this.$parent.title = "MIYA-CCO(京都水族館)";
                this.$parent.desc = "京都水族館のケープペンギンによるアイドルユニット";
            }
        }
    }
});

function toggleAll(){
    var ua = navigator.userAgent;
    if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
        var layout = document.getElementById('layout'),
            menu = document.getElementById('menu'),
            menuLink = document.getElementById('menuLink'),
            content = document.getElementById('main');

        var active = 'active';

        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);      
    }
}

function toggleClass(element, className) {
    var classes = element.className.split(/\s+/),
        length = classes.length,
        i = 0;

    for (; i < length; i++) {
        if (classes[i] === className) {
            classes.splice(i, 1);
            break;
        }
    }
    // The className is not found
    if (length === classes.length) {
        classes.push(className);
    }

    element.className = classes.join(' ');
}
