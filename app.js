const menuRoot = new Vue({
    el: '#layout',
    // template: `

    // `,
    data: function() {
        return{
            title: '(非公式)推しペン総選挙2018 各ペンギンデータ',
            desc: 'あなたの推しは何位？ メニューから推しペンを選択すると、グラフが表示されます。',
            target: '',
            target_rank: '',
            target_insta_rank: '',
            penguin_name: '',
            penguin_catch: '',
            edocco_member: [
                {
                    id: 'matsuri_oshipen',
                    name: 'まつり',
                    catch: '甘えんぼお嬢様',
                    rank: 7,
                    insta_rank: 3,
                },
                {
                    id: 'hanabi_oshipen',
                    name: 'はなび',
                    catch: '新人スタッフの登竜門',
                    rank: 11,
                    insta_rank: 7,
                },
                {
                    id: 'happi_oshipen',
                    name: 'はっぴ',
                    catch: 'お豆腐メンタル',
                    rank: 8,
                    insta_rank: 1,
                },
                {
                    id: 'taiko_oshipen',
                    name: 'たいこ',
                    catch: 'ダメンズ大好き',
                    rank: 4,
                    insta_rank: 2,
                },
                {
                    id: 'fuurin_oshipen',
                    name: 'ふうりん',
                    catch: 'イワシへのこだわり',
                    rank: 1,
                    insta_rank: 6,
                },
                {
                    id: 'wasshoi_oshipen',
                    name: 'わっしょい',
                    catch: '同級生の前では強気',
                    rank: 5,
                    insta_rank: 8,
                },
                {
                    id: 'anmitsu_oshipen',
                    name: 'あんみつ',
                    catch: 'ちびっこギャング',
                    rank: 10,
                    insta_rank: 15,
                },
                {
                    id: 'azuma_oshipen',
                    name: 'あずま',
                    catch: '守ってあげたい王子様',
                    rank: 9,
                    insta_rank: 11,
                },
                {
                    id: 'anko_oshipen',
                    name: 'あんこ',
                    catch: '1000年に1羽の美少女',
                    rank: 6,
                    insta_rank: 13,
                },
                {
                    id: 'chouchin_oshipen',
                    name: 'ちょうちん',
                    catch: '癒やしのぽっちゃりさん',
                    rank: 3,
                    insta_rank: 5,
                },
                {
                    id: 'kiriko_oshipen',
                    name: 'きりこ',
                    catch: '気品あふれる1歳児',
                    rank: 2,
                    insta_rank: 4,
                }
            ],
            miyacco_member: [
                {
                    id: 'shichijo_oshipen',
                    name: 'しちじょう',
                    catch: 'とどまる事なき好奇心',
                    rank: 10,
                    insta_rank: 18,
                },
                {
                    id: 'osshi_oshipen',
                    name: 'おっしー',
                    catch: '最速の逃げ足',
                    rank: 9,
                    insta_rank: 19,
                },
                {
                    id: 'take_oshipen',
                    name: 'たけ',
                    catch: 'ドのつくツンデレ',
                    rank: 2,
                    insta_rank: 14,
                },
                {
                    id: 'maru_oshipen',
                    name: 'まる',
                    catch: 'ときどき自宅警備員',
                    rank: 1,
                    insta_rank: 9,
                },
                {
                    id: 'ane_oshipen',
                    name: 'あね',
                    catch: '猛烈フードファイター',
                    rank: 8,
                    insta_rank: 22,
                },
                {
                    id: 'shijo_oshipen',
                    name: 'しじょう',
                    catch: 'ハンサムなイクメン',
                    rank: 5,
                    insta_rank: 17,
                },
                {
                    id: 'shinmachi_oshipen',
                    name: 'しんまち',
                    catch: 'ペンギンより人が好き',
                    rank: 3,
                    insta_rank: 10,
                },
                {
                    id: 'tera_oshipen',
                    name: 'てら',
                    catch: '魔性のクールビューティー',
                    rank: 6,
                    insta_rank: 16,
                },
                {
                    id: 'aya_oshipen',
                    name: 'あや',
                    catch: '父の一撃',
                    rank: 7,
                    insta_rank: 20,
                },
                {
                    id: 'hachi_oshipen',
                    name: 'はち',
                    catch: 'ザ肝っ玉母ちゃん',
                    rank: 11,
                    insta_rank: 21,
                },
                {
                    id: 'sen_oshipen',
                    name: 'せん',
                    catch: '鳥類だけど一匹狼',
                    rank: 4,
                    insta_rank: 12,
                },
            ]
            
        }
    },
    methods: {
        GetRank(penguin){
            var target_obj = this.target;
            var penguin = penguin;

            if (target_obj === 'edocco'){
                var keys = Object.keys(this.edocco_member);
                for (var i = 0; i < keys.length; i++) {
                    if (this.edocco_member[keys[i]].name === penguin) {
                        this.target_rank = this.edocco_member[keys[i]].rank;
                        this.target_insta_rank = this.edocco_member[keys[i]].insta_rank;
                        console.log('target_rank' + this.target_rank);
                    }
                }
            } else if (target_obj === 'miyacco'){
                var keys = Object.keys(this.miyacco_member);
                for (var i = 0; i < keys.length; i++) {
                    if (this.miyacco_member[keys[i]].name === penguin) {
                        this.target_rank = this.miyacco_member[keys[i]].rank;
                        this.target_insta_rank = this.miyacco_member[keys[i]].insta_rank;
                        console.log('target_rank' + this.target_rank);
                    }
                }
            }
         },

        ResetData(){
            this.title = '(非公式)推しペン総選挙 各ペンギンデータ';
            this.desc = 'メニューから推しペンを選択すると、グラフが表示されます。';
            this.target = '';
            this.penguin_name ='';
            this.penguin_catch = '';
            this.target_rank ='';
            this.target_insta_rank = '';
            var dom_obj = document.getElementById('chart');
            if (dom_obj !== null) dom_obj.innerHTML = '';
        },

        DrawPenguinChart(t_pen) {
            setTimeout(function() {
                var penguin_url = 'https://penguin-mall.com/get_penguin/get_penguin.php?t=' + t_pen;
                //console.log(penguin_url);
                var chart = c3.generate({
                    bindto: '#chart',
                    data: {
                        x: 'datetime',
                        xFormat: '%Y/%m/%d/%H:%M',
                        url: penguin_url,
                        mimeType: 'json',
                        type: 'bar',
                        types: {
                            'rank': 'line'
                        },
                        axes: {
                            follower: 'y',
                            rank: 'y2'
                        },
                        labels: false,
                        colors: {
                            'follower': '#b8e6e6',
                            'rank': '#708090'
                        }
                    },
                    axis: {
                        x: {
                            type: 'timeseries',
                            tick: {
                                culling: {
                                    max: 10
                                },
                                format: '%m/%d',
                            }
                        },
                        y: {
                            label: 'フォロワー数',
                            max: 1600
                        },
                        y2: {
                            label: '順位',
                            show: true,
                            inverted: true,
                            min: 1,
                            max: 22
                        }
                    },
                    size: {
                        height: 300
                    }
                });
            }, 1000);
        }, //DrawPenguinChart
    } //methods
});

setTimeout(function () {
    var chart = c3.generate({
        bindto: '#total_chart',
        data: {
            columns: [
                ['すみだ水族館', 10972],
                ['京都水族館', 5658],
            ],
            type: 'bar',
            labels: true,
            colors: {
                'すみだ水族館': 'DAB24D',
                '京都水族館': '#A75AC4'
            }
        },
        axis: {
            y: {
                label: 'フォロワー数'
            }
        },
        size: {
            height: 300
        }
    });
}, 1000);
